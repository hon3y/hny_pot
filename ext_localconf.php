<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/*
 * RealURL Hooks
 */
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['hny_pot'] =
        \HNY\HnyPot\Hooks\RealUrlAutoConfiguration::class . '->addConfig';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration']['hny_pot'] =
        \HNY\HnyPot\Hooks\RealUrlAutoConfiguration::class . '->postProcessConfiguration';
}
